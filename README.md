


1. Для создания таблиц выполнить миграции: yii migrate/up
2. Для создания таблиц RBAC: yii migrate --migrationPath=@yii/rbac/migrations
3. Для иницации параметров RBAC для приложения: yii rbac/init
4. Для заполнения тестовыми данными: yii test/addusers
5. Логин и пароль для админа admin admin