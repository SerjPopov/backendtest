<?php

namespace common\models;

use Yii;
use common\behaviors\ActiveRecordCachedBehavior;
use common\models\User;

/**
 * This is the model class for table "user_subscription".
 *
 * @property int $id
 * @property integer $date_end
 * @property string|null $comment
 */
class UserSubscription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_subscription';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'CachedBehavior' => [
                'class' => ActiveRecordCachedBehavior::class,
                'cacheKey' => [User::CACHE_ALL_USERS],
            ]
        ];
    }    
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_end'], 'safe'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_end' => 'Date End',
            'comment' => 'Comment',
        ];
    }
}
