<?php

namespace common\models\api;

use common\models\User;

class UserApi extends User
{

    public function fields()
    {
        return [
            'id',
            'username',
            'fio',
            'subscriptionDateEnd' => 'subscriptionDateEndaAsDate',
        ];
    }

}
