<?php

namespace common\models;

use yii\base\Model;
use common\models\User;

/**
 * UserEditForm
 */
class UserEditForm extends Model
{
    public $username;
    public $email;
    public $name;
    public $surname;
    public $patronymic;
    public $subscriptionDateEnd;
    public $password;

    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            //TODO сделать проверку на уникальность исключая само себя
            //['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данное имя пользователя уже существует.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'Имя пользователя должно содержать только латинские буквы и цифры'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],

            ['password', 'string'],
             
            ['name', 'required', 'when' => function ($model) { 
                return (($model->surname != '') || ($model->patronymic != ''));
            },
                'whenClient' => "function (attribute, value) { "
                . "return (($('#usereditform-surname').val() != '') || ($('#usereditform-patronymic').val() != ''));"
                . "}",
                'message' => 'Необходимо заполнить, если заполнено Surname или Patronymic',
            ],
                    
            ['surname', 'required', 'when' => function ($model) { 
                return (($model->name != '') || ($model->patronymic != ''));               
            },
                'whenClient' => "function (attribute, value) { "
                . "return (($('#usereditform-name').val() != '') || ($('#usereditform-patronymic').val() != ''));"
                . "}",
                'message' => 'Необходимо заполнить, если заполнено Name или Patronymic',
            ],

            ['patronymic', 'required', 'when' => function ($model) { 
                return (($model->name != '') || ($model->surname != ''));
            },
                'whenClient' => "function (attribute, value) { "
                . "return (($('#usereditform-name').val() != '') || ($('#usereditform-surname').val() != ''));"
                . "}",
                'message' => 'Необходимо заполнить, если заполнено Name или Surname',
            ],                     
            
            ['subscriptionDateEnd', 'safe'],
            
        ];
    }
    
    /**
     * Загружает в форму данные из модели User
     * @param User $user
     */
    public function loadFromUser(User $user)
    {
        $this->username = $user->username;
        $this->email = $user->email;
        $this->name = $user->name;
        $this->surname = $user->surname;
        $this->patronymic = $user->patronymic;
        $this->subscriptionDateEnd = $user->subscriptionDateEnd;
    }
    
}