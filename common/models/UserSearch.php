<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends User
{

    public $fio;
    public $subscriptionDateEnd;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
                [['id', 'status', 'created_at', 'updated_at'], 'integer'],
                [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'verification_token', 'name', 'surname', 'patronymic', 'fio', 'subscriptionDateEnd'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'username',
                'email',
                'fio'                 => [
                    'asc'  => ['surname' => SORT_ASC, 'name' => SORT_ASC, 'patronymic' => SORT_ASC],
                    'desc' => ['surname' => SORT_DESC, 'name' => SORT_DESC, 'patronymic' => SORT_DESC],
                ],
                'subscriptionDateEnd' => [
                    'asc'  => ['user_subscription.date_end' => SORT_ASC],
                    'desc' => ['user_subscription.date_end' => SORT_DESC],
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith(['subscription']);
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'status'     => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'auth_key', $this->auth_key])
                ->andFilterWhere(['like', 'password_hash', $this->password_hash])
                ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'verification_token', $this->verification_token])
                ->andWhere('name LIKE "%' . $this->fio . '%" ' . ' OR surname LIKE "%' . $this->fio . '%" ' . ' OR patronymic LIKE "%' . $this->fio . '%"');

        return $dataProvider;
    }

}
