<?php

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Behavior для очистки кеша при обновлении данных в базе данных
 */
class ActiveRecordCachedBehavior extends Behavior {

    public $cacheKey;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'deleteCache',
            ActiveRecord::EVENT_AFTER_UPDATE => 'deleteCache',
            ActiveRecord::EVENT_AFTER_DELETE => 'deleteCache',
        ];
    }

    public function deleteCache()
    {
        foreach ($this->cacheKey as $id) {
            Yii::$app->cache->delete($id);
        }
    }

}
