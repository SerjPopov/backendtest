<?php

return [
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        /*'cache'       => [
            'class'     => 'yii\caching\FileCache',
            'cachePath' => dirname(dirname(__DIR__)) . '/backend/runtime/cache',
        ],*/
        'cache' => [
            'class'   => 'yii\caching\MemCache',
            'useMemcached' => true,
            'servers' => [
                [
                    'host' => 'backendtest-memcached',
                    'port' => 11211,
                ]
            ]
        ],
        'formatter'   => [
            'dateFormat'     => 'php:d-m-Y',
            'datetimeFormat' => 'php:d.m.Y H:i:s',
            'timeFormat'     => 'php:H:i:s',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],
    ],
];
