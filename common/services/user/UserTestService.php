<?php

namespace common\services\user;

use Yii;
use common\models\User;
use common\models\UserSubscription;

/**
 * Сервис для наполнения тестовыми данными
 */
class UserTestService
{
    
    /**
     * Добавление тестовых юзеров
     */
    public function addTestUsers()
    {
        UserSubscription::deleteAll();
        User::deleteAll();
        
        $users = [
            [
                'username' => 'admin',
                'password' => 'admin',
                'role' => 'admin',
                'email' => 'admin@email.com',
                'name' => '',
                'surname' => '',
                'patronymic' => '',
            ],            
            [
                'username' => 'user1',
                'password' => 'user1',
                'role' => 'user',
                'email' => 'email_1_@email.com',
                'name' => 'Семен',
                'patronymic' => 'Иванович',
                'surname' => 'Петров',                
                'date_end' => 1588370399,
            ],
            [
                'username' => 'user2',
                'password' => 'user2',
                'role' => 'user',
                'email' => 'email_2_@email.com',
                'name' => 'Петр',
                'patronymic' => 'Сергеевич',
                'surname' => 'Антонов',         
            ],
            [
                'username' => 'user3',
                'password' => 'user3',
                'role' => 'user',
                'email' => 'email_3_@email.com',
                'name' => 'Алла',
                'patronymic' => 'Ивановна',
                'surname' => 'Золотухина',
                'date_end' => 1588197599,               
            ],            
        ];
        
        foreach ($users as $user) {
            $this->addUser($user);
        }
    }
    
    /**
     * Добавление пользователя по данными из массива $user
     * @param array $userData
     */
    private function addUser($userData)
    {
        if (isset($userData['date_end'])) {
            $subscription = new UserSubscription();
            $subscription->date_end = $userData['date_end'];
            $subscription->save();            
        }

        
        $user = new User();
        $user->username = $userData['username'];
        $user->email = $userData['email'];
        $user->name = $userData['name'];
        $user->surname = $userData['surname'];
        $user->patronymic = $userData['patronymic'];
        $user->subscription_id = (isset($subscription->id)) ? $subscription->id : NULL;
        $user->status = User::STATUS_ACTIVE;
        $user->setPassword($userData['password']);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->save();
        
        $auth = Yii::$app->authManager;
        if (isset($userData['role'])) {
            $userRole = $auth->getRole($userData['role']);           
        } else {
            $userRole = $auth->getRole('user');
        }
        $auth->assign($userRole, $user->getId());  
    }
    
}
