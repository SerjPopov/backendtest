<?php

namespace common\services\user;

use Exception;
use Yii;
use common\models\User;
use common\models\UserEditForm;
use common\models\UserSubscription;

/**
 * Сервис для работы с пользователями
 */
class UserService
{
    /**
     * Обновление данных пользователя $user по данным из формы
     * @param UserEditForm $userEditForm
     * @param User $user
     * @return boolean
     * @throws Exception
     */
    public function updateUserFromForm(UserEditForm $userEditForm, User $user)
    {
        $user->username = $userEditForm->username;
        $user->username = $userEditForm->username;
        $user->email = $userEditForm->email;
        $user->name = $userEditForm->name;
        $user->surname = $userEditForm->surname;
        $user->patronymic = $userEditForm->patronymic;
        if (isset($userEditForm->password)) {
            $user->setPassword($userEditForm->password);
            $user->generateAuthKey();
        }
        if (!$user->save()) {
            return false;
        }
        if ($userEditForm->subscriptionDateEnd != '') {
            $dateTime = new \DateTime($userEditForm->subscriptionDateEnd . ' 23:59:59');
            $subscriptionDateEndTimeStamp = Yii::$app->formatter->asTimestamp($dateTime);
            return $this->updateUserSubscription($subscriptionDateEndTimeStamp, $user);
        } else {
            if (isset($user->subscriptionDateEnd)) {
                $user->subscription->delete();
                $user->save();
            }
            return true;
        }
    }

    /**
     * Обновление подписки
     * @param int $subscriptionDateEndTimeStamp
     * @param User $user
     * @return boolean
     */
    private function updateUserSubscription(int $subscriptionDateEndTimeStamp, User $user)
    {
        if (isset($user->subscription)) {
            $user->subscription->date_end = $subscriptionDateEndTimeStamp;
            return $user->subscription->save();
        } else {
            $subscription = new UserSubscription();
            $subscription->date_end = $subscriptionDateEndTimeStamp;
            if ($subscription->save()) {
                $user->subscription_id = $subscription->id;
                return $user->save();
            } else {
                return false;
            }
        }
    }

}
