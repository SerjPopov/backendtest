<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

//use \app\rbac\UserGroupRule;

class RbacController extends Controller
{
    /**
     * Инициация настроек
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        
        $roleAdmin = $auth->createRole('admin');
        $roleAdmin->description = 'Администратор';
        $auth->add($roleAdmin);

        $roleUser = $auth->createRole('user');
        $roleUser->description = 'Пользователь';
        $auth->add($roleUser);
    }

}
