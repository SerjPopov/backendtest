<?php

namespace console\controllers;

use yii\console\Controller;

class TestController extends Controller
{
    /**
     * Добавление тестовых данных в таблицу user
     */
    public function actionAddusers()
    {
        (new \common\services\user\UserTestService())->addTestUsers();
    }

}
