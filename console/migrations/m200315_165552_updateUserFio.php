<?php

use yii\db\Migration;

/**
 * Class m200315_165552_updateUserFio
 */
class m200315_165552_updateUserFio extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'name', $this->string()->defaultValue(null));
        $this->addColumn('{{%user}}', 'surname', $this->string()->defaultValue(null));
        $this->addColumn('{{%user}}', 'patronymic', $this->string()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200315_165552_updateUserFio cannot be reverted.\n";

        return false;
    }

}
