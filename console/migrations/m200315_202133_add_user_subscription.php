<?php

use yii\db\Migration;

/**
 * Class m200315_202133_add_user_subscription
 */
class m200315_202133_add_user_subscription extends Migration
{
    
    const TABLE_NAME = '{{%user_subscription}}';    
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id'            => $this->bigPrimaryKey(),
                'date_end'      => $this->integer()->notNull(),
                'comment'       => $this->string(),
            ]
        );
        $this->createIndex('date_end', self::TABLE_NAME, 'date_end'); 
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200315_202133_add_user_subscription cannot be reverted.\n";

        return false;
    }
}
