<?php

use yii\db\Migration;

/**
 * Class m200315_210507_alter_user_subscription
 */
class m200315_210507_alter_user_subscription extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'subscription_id', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200315_210507_alter_user_subscription cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200315_210507_alter_user_subscription cannot be reverted.\n";

        return false;
    }
    */
}
