<?php

namespace api\controllers;

use Yii;
use yii\rest\Controller;
use common\models\api\UserApi;
use common\models\User;
use common\models\UserEditForm;
use common\services\user\UserService;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\AccessControl;

class UserController extends Controller
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                    [
                    'allow'   => true,
                    'actions' => ['index', 'view'],
                    'roles'   => ['user', 'admin'],
                ],
                    [
                    'allow'   => true,
                    'actions' => ['update'],
                    'roles'   => ['admin'],
                ],
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return UserApi::findAllUsers();
    }

    public function actionView($id)
    {
        return $this->findModelApi($id);
    }

    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $form = new UserEditForm();

        $params = Yii::$app->request->getBodyParams();

        if ($form->load($params, '') && $form->validate() &&
                (new UserService())->updateUserFromForm($form, $user)) {
            return ['id' => $id, 'result' => 'ok'];
        } else {
            return ['id' => $id, 'result' => 'error', 'errors' => $form->getErrors()];
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserApi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelApi($id)
    {
        if (($model = UserApi::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Пользователь не найден');
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Пользователь не найден');
    }

}
