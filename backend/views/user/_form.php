<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $form common\models\UserEditForm */
/* @var $activeForm yii\widgets\ActiveForm */
?>

<div class="user-form row">
    <?php $activeForm = ActiveForm::begin(); ?>

    <div class="col-md-6">
        <?= $activeForm->field($form, 'username')->textInput() ?>
        <?= $activeForm->field($form, 'password')->passwordInput() ?>
        <?= $activeForm->field($form, 'email')->textInput() ?>         
    </div>

    <div class="col-md-6">
        <?= $activeForm->field($form, 'surname')->textInput() ?>
        <?= $activeForm->field($form, 'name')->textInput() ?>
        <?= $activeForm->field($form, 'patronymic')->textInput() ?>        
    </div>

    <div class="col-md-12">

        <?=
        $activeForm->field($form, 'subscriptionDateEnd')->widget(DatePicker::classname(), [
            'options'       => [
                'placeholder' => 'Укажите дату завершения подписки',
                'value'       => !empty($form->subscriptionDateEnd) ? Yii::$app->formatter->asDate($form->subscriptionDateEnd) : '',
            ],
            'pluginOptions' => [
                'autoclose' => true,
                'format'    => 'dd-mm-yyyy'
            ],
        ])
        ?>      

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>        
    </div>

<?php ActiveForm::end(); ?>

</div>
